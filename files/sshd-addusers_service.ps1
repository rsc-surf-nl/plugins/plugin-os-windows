﻿$LOGFILE = "C:\logs\sshd-update_key.log"
$Admingroupname = "src_co_admin"

Function LogWrite {
   Param ([string]$logstring)
   '{0:u}: {1}' -f (Get-Date), $logstring | Out-File $Logfile -Append
}

function get-src_users_sshkeys($Admingroupname) {
    $CO_USER_API_ENDPOINT = (Get-ItemProperty -Path HKLM:\Software\ResearchCloud -ErrorAction SilentlyContinue).co_user_api_endpoint
    $CO_TOKEN = (Get-ItemProperty -Path HKLM:\Software\ResearchCloud -ErrorAction SilentlyContinue).co_token
    try {
        $CO_USER_INFO = Invoke-RestMethod -Uri $CO_USER_API_ENDPOINT -Method Get -Headers @{ "Authorization" = $CO_TOKEN } -ContentType "application/json" -UseBasicParsing
    }
    catch {
        return "$_"
	    break
    }

    $CO_USER_SSHKey = New-Object PSObject
    foreach ($username in $CO_USER_INFO.username) {
        
        $usersshkey = ($CO_USER_INFO | Where-Object {$_.username -eq $username}).ssh_keys
        $useradmin = ($CO_USER_INFO | Where-Object {$_.username -eq $username}).roles
        if ($useradmin -like $Admingroupname) {
            $isadmin = $true
        } else {
            $isadmin = $false
        }
        $CO_USER_SSHKey | Add-Member -Name $username -Value "$usersshkey,$isadmin" -MemberType NoteProperty
    }
    return $CO_USER_SSHKey
}

function new-local_user($user) {
    $secpasswd = ConvertTo-SecureString -String "$(-Join("ABCDabcd!&@#$%1234567890".tochararray() | Get-Random -Count 25 | ForEach-Object {[char]$_}))" -AsPlainText -Force
    New-ADUser -Name $user -Description 'Autocreated account by SRC sshd script.' -PasswordNeverExpires $true -SamAccountName $user -Enabled $true -UserPrincipalName $user -AccountPassword $secpasswd
}

function update-admin-group($user, $admin, $adgroup) {
    $usrmemberadmin = [bool](Get-ADGroupMember -Identity $adgroup | Where-Object {$_.name -eq $user})
    if ($admin) {
        #Add the user only if the users is NOT member of the admin group
        if (!($usrmemberadmin)) {
            LogWrite("ADD user $user to the group $adgroup")
            Add-ADGroupMember -Identity $adgroup -Members $user -Confirm:$false
        }
    } else {
        #Remove the user only if the user IS member of the admin group
        if ($usrmemberadmin) {
            Remove-ADGroupMember -Identity $adgroup -Members $user -Confirm:$false
            LogWrite("REMOVE user $user from group $adgroup")
        }
    }
}

function add-sshkey($user, $sshkey, $sshpath) {
    # Add the user to the registry ssh_users
    $currentValue = (Get-ItemProperty -Path $registryPath -Name "ssh_users" -ErrorAction SilentlyContinue).ssh_users
    if (-not $currentValue) {
        $currentValue = ""
    }
    $currentUsers = $currentValue -split ";"
    if ($currentUsers -notcontains $user) {
        if ($currentValue -ne "") {
            $currentValue += ";"
        }
        $currentValue += $user
    }
    # Update the ssh_users property with the new value
    Set-ItemProperty -Path $registryPath -Name "ssh_users" -Value $currentValue
    $updateneed = $false
    #check if .ssh folder exist
    if (!(test-path "$sshpath\authorized_keys" -ErrorAction SilentlyContinue -WarningAction SilentlyContinue)) {
        LogWrite("CREATE authorized_key for user: $user")
        $updateneed = $true
    } else {
        #compare current SSH authorized_keys
        $sshfilecontent = (Get-Content -Path "$sshpath\authorized_keys") -join ' '
        if (Compare-Object $sshfilecontent $sshkey) {
            LogWrite("UPDATE authorized_keys file for user: $user")
            $updateneed = $true
        }
    }
    #update ssh if needed
    if ($updateneed) {
        $sshkeys = $usersshkey -replace " ssh-", "`nssh-"
        New-Item -Path "$sshpath\authorized_keys" -Value $sshkeys -Force
    }
}

function disable-user($user) {
    # remove the user from the registry ssh_users
    $currentUsers = ((Get-ItemProperty -Path $registryPath -Name "ssh_users" -ErrorAction SilentlyContinue).ssh_users) -split ";"
    $currentUsers = $currentUsers | Where-Object { $_ -ne $user }
    $currentValue = $currentUsers -join ";"
    # Update the ssh_users property with the new value
    Set-ItemProperty -Path $registryPath -Name "ssh_users" -Value $currentValue
    Disable-ADAccount -Identity $user
}

function New-UserProfile {
<#
.EXAMPLE
    New-UserProfile -DomainName MyLab -UserName MyUser -Verbose
#>
[cmdletbinding()]
[OutputType([System.IO.DirectoryInfo])]
param(
    [Parameter(Mandatory=$true)]
    [String]$UserName,

    [String]$DomainName = $env:USERDOMAIN
)
$PSDefaultParameterValues = @{'Get-CimInstance:Verbose' = $false}

try {
    Write-Verbose "Look up SID for user $UserName..."
    $objUser  = [Security.Principal.NTAccount]::new($DomainName, $UserName)
    $strSID   = $objUser.Translate([Security.Principal.SecurityIdentifier])
    $SID      = $strSID.Value
}
catch {Write-Warning "Could not find SID for $UserName" ; throw $_}

$code = @'
    using System;
    using System.Runtime.InteropServices;
    public static class PInvoke {
        [DllImport("userenv.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int CreateProfile( [MarshalAs(UnmanagedType.LPWStr)] String pszUserSid, [MarshalAs(UnmanagedType.LPWStr)] String pszUserName, [Out, MarshalAs(UnmanagedType.LPWStr)] System.Text.StringBuilder pszProfilePath, uint cchProfilePath);
    }
'@
$sb  = [System.Text.StringBuilder]::new(260)
$len = $sb.Capacity

try   {Add-Type -TypeDefinition $code -ErrorAction Stop}
catch {Write-Warning 'Could not load pinvoke method' ; throw $_}

Write-Verbose "Create profile for user $UserName..."
try   {$result = [PInvoke]::CreateProfile($SID, $UserName, $sb, $len)}
catch {throw $_.Exception.Message}

switch($result) {
    '-2147024713'  {Write-Warning "Profile already exists for $userName" ; break}
    '-2147024809'  {Write-Warning "$username not found"                  ; break}
    '-2147023582'  {Write-Warning 'Please run this elevated'             ; return}
    0              {Write-Verbose "Profile created for user $UserName"   ; break}
    default        {Write-Warning 'Unknown result'                       ; break}
}

if ($result -eq 0 -or $result -eq '-2147024713') {
    Write-Verbose "Find the profile path for $UserName..."
    $Path = (Get-CimInstance  Win32_UserProfile | Where-Object SID -eq $SID).LocalPath

    if (Test-Path $Path) {Get-Item $Path}   # <-- this is the output, should be [System.IO.DirectoryInfo]
    else {throw "Could not find profile $Path"}
}

}

function Main {
    #get ssh key from SRC
    $CO_USER_SSHKey = get-src_users_sshkeys $Admingroupname
    $useraccounts = ($CO_USER_SSHKey | Get-Member | Where-Object {$_.MemberType -eq 'NoteProperty'}).Name

    foreach ($user in $useraccounts) {

        $usersshkey = $CO_USER_SSHKey.$user.Split(",")[0]
        $userisadmin = $CO_USER_SSHKey.$user.Split(",")[1]
        if ($userisadmin -eq 'True') {
            $userisadmin = $true
        } else {
            $userisadmin = $false
        }

        #only add the key if the user has a key configured in SRAM
        if ($usersshkey){
            #check if the user exist and create if not exist or enable when disabled
            if (![bool] (Get-ADUser -Filter { SamAccountName -eq $user }).Enabled) {
                if (((Get-ADUser -Filter { SamAccountName -eq $user }).Enabled) -eq $false) {
                    LogWrite("ENABLE useraccount $user")
                    Enable-ADAccount -Identity $user
                } else {
                    LogWrite("CREATE useraccount $user")
                    new-local_user $user
                }
            }
            #check if user has a profile
            $localuserpath = (Get-CimInstance Win32_UserProfile -Filter "SID = '$((Get-ADUser $user).Sid)'").LocalPath
            if (!($localuserpath)) {
                
                New-UserProfile -UserName $user #-Verbose
            } 
            #update the SSH key to each user
            $localuserpath = (Get-CimInstance Win32_UserProfile -Filter "SID = '$((Get-ADUser $user).Sid)'").LocalPath
            if ($localuserpath) {
                $sshpath = "$localuserpath\.ssh"
                add-sshkey $user $usersshkey $sshpath
            }
        }
        #Update ad admin group for existing user
        if ([bool] (Get-ADUser -Filter { SamAccountName -eq $user })) {
            update-admin-group $user $userisadmin $Admingroupname
        }
    }
    #Disable users that have been added by SSH but are not member of the SRAM CO
    $regcurrentUsers = ((Get-ItemProperty -Path $registryPath -Name "ssh_users" -ErrorAction SilentlyContinue).ssh_users) -split ";"
    foreach ($regcurrentUser in $regcurrentUsers) {
        if ($useraccounts -notcontains $regcurrentUser) {
            LogWrite("DISBALE useraccount $regcurrentUser")
            disable-user $regcurrentUser
        }
    }
}

Main