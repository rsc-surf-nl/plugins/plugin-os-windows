

$LOGFILE = "c:\logs\post-install-script.log"

Function Write-Log([String] $logText) {

    '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append

}


function Optimize-PowershellAssemblies {
    # NGEN powershell assembly, improves startup time of powershell by 10x
    $old_path = $env:path
    try {
      $env:path = [Runtime.InteropServices.RuntimeEnvironment]::GetRuntimeDirectory()
      [AppDomain]::CurrentDomain.GetAssemblies() | ForEach-Object {
        if (! $_.location) {continue}
        $Name = Split-Path $_.location -leaf
        if ($Name.startswith("Microsoft.PowerShell.")) {
          Write-Progress -Activity "Native Image Installation" -Status "$name"
          ngen install $_.location | ForEach-Object {"`t$_"}
        }
      }
    } finally {
      $env:path = $old_path
    }
  }




Function Main {

    try {

      if(!(Test-Path -Path c:\logs)){
        New-Item -ItemType Directory -Path c:\logs 
        icacls c:\logs /grant Users:F /T
      }

        Write-Log "Log directory created."
    }
    catch {
        Write-Log "$_"

        Throw $_

    }

    Write-Log "Disable server manager start at startup"
    #disable server manager start at startup
    try {
        Get-ScheduledTask -TaskName ServerManager | Disable-ScheduledTask -Verbose
    }
    catch {
        Write-Log "$_"

        Throw $_
    }
    #disable Network discovery
    Write-Log "Disable network discovery"
    try {
        netsh advfirewall firewall set rule group="Network Discovery" new enable=No
        Get-NetConnectionProfile | Set-NetConnectionProfile -NetworkCategory Private -Confirm:$false -PassThru
    }
    catch {
        Write-Log "$_"

        Throw $_
    }

    Write-Log "Set time server (Local CMOS Clock) with initial sync"
    try {
      w32tm /config /manualpeerlist:time.windows.com /syncfromflags:manual /reliable:yes /update
      w32tm /resync
      #Write-Log "Set time zone UTC"
      Set-TimeZone -Id "UTC"
      w32tm /config /syncfromflags:domhier /update
      Restart-Service w32time
      new-itemproperty -path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce -name "SET TIMEZONE" -value '"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -c Set-TimeZone -Id ''W. Europe Standard Time'''
    }
    catch {
        Write-Log "$_"

        Throw $_
    }

    Write-Log "Optimize-PowershellAssemblies"
    Optimize-PowershellAssemblies


    Write-Log "Finished"
}



Main    
