$LOGFILE = "c:\logs\mount-disks.log"

Function Write-Log([String] $logText) {

    '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append

}

Function Format-Disks() {

    Write-Log "Check RAW disk for Format.."

    $disks = Get-Disk | Where-Object PartitionStyle -eq 'RAW' | Sort-Object Number
    $letters = 70..89 | ForEach-Object { [char]$_ }
    $count = 1
    ForEach ($disk in $disks) {
        Write-Log "Initializing format disk $disk."

        $partitions =  Get-Partition | Where-Object Disk -Eq $disk 
        $countPartion = 0
        foreach ($partion in $partitions) {
            $countPartion++
        }

        if($countPartion -Eq 0){
            Stop-Service -Name ShellHWDetection
            if ($volume_mount_no_name -eq 'true') {
                $driveLetter = $letters[$count].ToString()
                $label = "volume_$count"
    
                $disk | Initialize-Disk -PartitionStyle GPT -PassThru | New-Partition -UseMaximumSize -DriveLetter $driveLetter | Format-Volume -FileSystem NTFS -NewFileSystemLabel $label -Confirm:$false -Force
            } else {
                $disk | Initialize-Disk -PartitionStyle GPT -PassThru | New-Partition -UseMaximumSize -AssignDriveLetter | Format-Volume -FileSystem NTFS -Confirm:$false -Force
            }
            Start-Service -Name ShellHWDetection
            Write-Log "Disk $disk initialized."
            $count++
        }
    }

}

Function Set-Disks-Online() {
    Write-Log "Set-Disks-Online"

    $disks =  Get-Disk | Where-Object IsOffline -Eq $True 
    
    ForEach ($disk in $disks) {

        Write-Log "Set online disk $disk."

        Stop-Service -Name ShellHWDetection

        $disk | Set-Disk -IsOffline $False
        
        $disk | Set-Disk -IsReadonly $False 

        Start-Service -Name ShellHWDetection

        Write-Log "Disk $disk set online."
    }

}

Function Set-Disk-acls() {
    Write-Log "Set-acls"

    $Disks = Get-Volume | Select-Object -ExpandProperty Driveletter 

    foreach($disk in $disks){
        $volume = Get-Volume -DriveLetter $disk
        $driveLetter = $volume.DriveLetter
        $drivetype = $volume.DriveType
        if( $DriveLetter -ne 'C' -AND $DriveLetter -ne 'A' -AND $drivetype -ne 'CD-ROM'){
            #icacls $driveLetter':' /grant Users:F /T
            $drivepath=$driveletter+":"
            $ACL = Get-Acl -Path $drivepath
            $AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("Users", "FullControl", "ContainerInherit, ObjectInherit", "None", "Allow")
            $ACL.SetAccessRule($AccessRule)
            $ACL | Set-Acl -Path $drivepath
        }
    } 
 } 

Function get-volumename($drive_serial) {
    # Parse storage data to get required values
    $co_token = (Get-ItemProperty -Path HKLM:\Software\ResearchCloud).co_token
    $workspace_id = (Get-ItemProperty -Path HKLM:\Software\ResearchCloud).workspace_id
    $storage_api_endpoint = (Get-ItemProperty -Path HKLM:\Software\ResearchCloud).storage_api_endpoint
    $storage_api_endpoint_workspace = $storage_api_endpoint+$workspace_id+'/'
    
    # Getting volume name by API
    $attempt = 10
    $success = $false
    while ($attempt -gt 0 -and -not $success) {
        try {
        $workspaceinfo = Invoke-RestMethod -Uri $storage_api_endpoint_workspace -Method Get -Headers @{ "Authorization" = $co_token } -ContentType "application/json" -UseBasicParsing
        $success = $true
        } catch {
        # remember error information
        Write-Log "$_"
        Throw $_
        $attempt--
        }
    }
    foreach ($storage in $workspaceinfo.meta.storages) {
        $volnrchar = ($drive_serial | Measure-Object -Character).Characters
        $volumeid_short = $storage.volume_id.Substring(0, $volnrchar)
        if ($drive_serial -eq $volumeid_short) {
            return $storage.name
        }
    }    
}

Function Set-Disk-VolumeNames {
    Write-Log "Check volume names to change.."
    
    $disks = Get-Disk | Where-Object {$_.IsBoot -eq $false}
    $i=0
    ForEach ($disk in $disks) {
        $partitions = $disk | Get-Partition | Where-Object {$_.DriveLetter -and $_.IsSystem -eq $false -and $_.IsBoot -eq $false}
        $partitioncount = ($partitions | Measure-Object).count

        ForEach ($partition in $partitions) {
            $diskserial = ($partition | Get-Disk).SerialNumber
            $volume_active = $partition | get-volume
            $volumename = $volume_active.FileSystemLabel
            $src_label = get-volumename $diskserial
            if (!($src_label)) {
                $src_label = "volume_"+($partition | Get-Disk).number
            }

            if ($partitioncount -gt 1) {
                $i++
                $src_label = $src_label+"_$i"
            }
            if ($src_label.Length -gt 32) {
                $src_label = $src_label.Substring(0, 32) #subtract to 32 characters
            }
            if ($volumename -ne $src_label) {
                Write-Log "Disk " $disk.Number $volumename "is changed to $src_label"
                Set-Volume -InputObject $volume_active -NewFileSystemLabel $src_label
            }
        }
    }
}

Function Main {
    $volume_mount_no_name = (Get-ItemProperty -Path HKLM:\Software\ResearchCloud).volume_mount_no_name
    try {
        if(!(Test-Path -Path c:\logs)){
            New-Item -ItemType Directory -Path c:\logs 
            icacls c:\logs /grant Users:F /T
        }

        Write-Log "Log directory created."

    }

    catch {
        Write-Log "$_"

        Throw $_

    }

    try {
        Format-Disks    
    }
    catch {
        Write-Log "$_"

        Throw $_
    }
    

    # set offline disks online
    try {
        Set-Disks-Online
    }
    catch {
        Write-Log "$_"

        Throw $_
    }

    try {
        Set-Disk-acls    
    }
    catch {
        Write-Log "$_"

        Throw $_
    }
    try {
        if ($volume_mount_no_name -ne 'true') {
            Set-Disk-VolumeNames
        }
    }
    catch {
        Write-Log "$_"

        Throw $_
    }
  
}

Main
