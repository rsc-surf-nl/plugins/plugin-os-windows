#SRC Log Rotation
$Logfolder = $env:SystemDrive + "\logs"
$Rotatesize = 10MB
$Rotatenr = 3
$Logfiletxt = "C:\logs\src-log-cleanup.log"

Function LogWrite {
   Param ([string]$logstring)
   '{0:u}: {1}' -f (Get-Date), $logstring | Out-File $Logfiletxt -Append
}

function RotateLog($Logfolder, $Rotatesize, $Rotatenr) {
    $logs = Get-ChildItem $Logfolder | Where-Object {$_.extension -in ".log",".txt"}
    ForEach ($logfile in $logs) {
        
        $logfullpath = $Logfolder + "\" + $logfile
        
        #check if the log file is bigger then the rotationsize
        if ($logfile.Length -ge $Rotatesize) {
            LogWrite("$logfile is bigger than $Rotatesize KB")

            #remove last rotation file if exist
            if (Test-Path "$logfullpath.$Rotatenr.zip") {
                LogWrite("remove rotation file $logfile.$Rotatenr.zip")
                remove-Item "$logfullpath.$Rotatenr.zip"
            }

            #move each log file up one
            $r=$Rotatenr
            for ($i=0; $r -ge $i; $r--) {
                if (Test-Path "$logfullpath.$r.zip") {
                    $rn = $r+1
                    LogWrite("$logfile.$r.zip -> $logfile.$rn.zip")
                    Move-Item -Path "$logfullpath.$r.zip" -Destination "$logfullpath.$rn.zip" -Force
                }
            }
            $i=$r

            #Move the log file to archive
            $rtnr=$Rotatenr
            for ($i=0; $i -le $rtnr; $i++) {
                if (!(Test-Path "$logfullpath.$i.zip")) {
                    LogWrite("Compress $logfile to $Logfolder\$logfile.$i.zip")
                    Compress-Archive -Path "$logfullpath" -DestinationPath "$logfullpath.$i.zip"
                    Clear-Content "$logfullpath" -Force
                    LogWrite("-------------------------------------------------------------------")
                    $i=$rtnr
                }
            }
        }
    }
}

RotateLog $Logfolder $Rotatesize $Rotatenr