$taskname = 'SRC-MountDisks'
$class = Get-CimClass MSFT_TaskEventTrigger root/Microsoft/Windows/TaskScheduler
$trigger = $class | New-CimInstance -ClientOnly
$trigger.Enabled = $true
$trigger.Subscription = '<QueryList><Query Id="0" Path="Microsoft-Windows-StorageSpaces-Driver/Operational"><Select Path="Microsoft-Windows-StorageSpaces-Driver/Operational">*[System[Provider[@Name=''Microsoft-Windows-StorageSpaces-Driver''] and EventID=207]]</Select></Query></QueryList>'
$ActionParameters = @{
    Execute  = 'C:\Windows\system32\WindowsPowerShell\v1.0\powershell.exe'
    Argument = '-NoProfile -WindowStyle Hidden -File C:\src-scripts\mount-disks.ps1'
}
$Action = New-ScheduledTaskAction @ActionParameters
$Principal = New-ScheduledTaskPrincipal -UserId 'NT AUTHORITY\SYSTEM' -LogonType ServiceAccount
$Settings = New-ScheduledTaskSettingsSet
$RegSchTaskParameters = @{
    TaskName    = $taskname
    Description = 'Run a mount-disks script'
    Action      = $Action
    Principal   = $Principal
    Settings    = $Settings
    Trigger     = $Trigger
}
Register-ScheduledTask @RegSchTaskParameters
#Get-ScheduledTask -TaskName SRC-MountDisks | New-ScheduledTaskTrigger -AtStartup

# Add extra trigger on boot
$triggers  = @()
$triggers += (get-scheduledtask $taskname).Triggers
$trigger = New-ScheduledTaskTrigger -AtStartup
$trigger.Delay = "PT30S"
$triggers += $trigger
Set-ScheduledTask -TaskName $taskname -Trigger $triggers